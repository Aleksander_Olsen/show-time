package com.kaglainc.showtime;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;


public class CalendarFragment extends Fragment {

    private View view;
    private Calendar mCalendar;

    private CalendarAdapter adapter;

    public CalendarFragment() {
        // Required empty public constructor
    }

    public void setMember(Calendar calendar){
        mCalendar = calendar;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this fragment across configuration changes.
        setRetainInstance(true);

    }

    /**
     * Update after new month.
     */
    void refreshCalendar()
    {
        TextView title  = (TextView) view.findViewById(R.id.title);

        adapter.refreshDays();
        adapter.notifyDataSetChanged();
        updateCalendar();

        title.setText(android.text.format.DateFormat.format("MMMM yyyy", mCalendar));
    }

    /**
     * Creates fragment view.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return Finished calendarview for container.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_calendar, container, false);

        adapter = new CalendarAdapter(getActivity(), mCalendar);

        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        gridview.setAdapter(adapter);

        //Find episodes per day.
        updateCalendar();

        TextView title  = (TextView) view.findViewById(R.id.title);
        title.setText(android.text.format.DateFormat.format("MMMM yyyy", mCalendar));

        TextView previous  = (TextView) view.findViewById(R.id.previous);
        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCalendar.add(Calendar.MONTH, -1);
                refreshCalendar();
            }
        });

        TextView next  = (TextView) view.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCalendar.add(Calendar.MONTH, 1);
                refreshCalendar();

            }
        });

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                TextView date = (TextView)v.findViewById(R.id.date);
                if(!date.getText().equals("")) {

                    int day = Integer.valueOf(date.getText().toString());
                    ((PersonalListActivity)getActivity()).setListFragment(adapter.getItem(day-1));

                }

            }
        });

        return view;
    }

    /**
     * Find episodes per day
     */
    void updateCalendar(){
        ((PersonalListActivity)getActivity()).setCurMonth(null);
        ArrayList<Show> shows;
        DataSource datasource = new DataSource(getActivity());
        datasource.open(); // Open database
        shows = datasource.getShows(); // Get all shows in database

        // If any saved shows.
        if(shows.size() > 0) {
            // For each day of the month
            for (int i = 0; i < mCalendar.getMaximum(Calendar.DAY_OF_MONTH); i++) {
                //Create the string used for date comparisons
                String month = (mCalendar.get(Calendar.MONTH) < 9 ? "0" : "") + String.valueOf(mCalendar.get(Calendar.MONTH) + 1);
                String date = String.valueOf(mCalendar.get(Calendar.YEAR)) + "-" + month + "-" + (i + 1 < 10 ? "0" + String.valueOf(i + 1) : String.valueOf(i + 1));

                //Get all episodes scheduled to air that date
                ArrayList<Episode> episodes = datasource.getEpisodesByDate(date);

                // Iterates through each show.
                for (Show show : shows) {

                    int j = 0;
                    for (; j < episodes.size(); j++) {
                        if (show.getId() == episodes.get(j).getSeries()) {
                            episodes.get(j).setSeriesName(show.getName());
                        }
                    }
                }

                // If any episodes found for this day, add it to the adapter.
                if(episodes.size() > 0) {
                    adapter.addItems(i, episodes);
                    ((PersonalListActivity)getActivity()).addToMonth(episodes);
                    adapter.notifyDataSetChanged();
                }
            }
        }
        datasource.close(); // Close database

        //If valid connection
        if(((PersonalListActivity) getActivity()).getReceiver().isConnected(getActivity())) {

            // Add episodes for next month if not updated to database.
            Calendar calendar = (Calendar) mCalendar.clone();
            calendar.add(Calendar.MONTH, 1);
            updateDB(calendar, shows);

            // Add episodes for previous month if not updated to database.
            calendar = (Calendar) mCalendar.clone();
            calendar.add(Calendar.MONTH, -1);
            updateDB(calendar, shows);
        }

    }

    // Update episodes for a month
    void updateDB(Calendar calendar, ArrayList<Show> shows) {

        EpisodeUpdater updater = new EpisodeUpdater(calendar, getActivity(), shows);
        updater.execute();

    }
}
