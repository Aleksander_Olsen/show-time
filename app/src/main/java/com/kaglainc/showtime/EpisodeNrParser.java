package com.kaglainc.showtime;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Aleksander, Karl Erik, Glen on 02.12.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
/**
 * This xml parser is used to find the number of episodes in a season.
 * It goes through the entire xml with all episodes for the entire show.
 * It then checks if the season number is the same as the season that is going to display.
 * If so, increase the total number of episodes in the season.
 * The parser will return a number with the total number of episodes.
 */
class EpisodeNrParser {
    private static final String ns = null;
    private String seasonNr = null;
    private Integer totalEpisodes = 0;

    public Integer parse(InputStream in, String _seasonNr) throws XmlPullParserException, IOException {
        seasonNr = _seasonNr;
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private Integer readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        Integer number = 0;

        parser.require(XmlPullParser.START_TAG, ns, "Data");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if(name.equals("Episode")) {
                number = readEpisode(parser);
            } else {
                skip(parser);
            }
        }
        return number;
    }

    private Integer readEpisode(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "Episode");
        Integer number = -1;
        Integer season = -1;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String parsed = parser.getName();

            if (parsed.equals("EpisodeNumber")) {
                number = readEpisodeNr(parser);
            } else if (parsed.equals("SeasonNumber")) {
                season = readSeason(parser);
            } else {
                skip(parser);
            }


            if(season == Integer.parseInt(seasonNr) && totalEpisodes < number) {
                totalEpisodes = number;
            }
        }

        return totalEpisodes;
    }

    private Integer readEpisodeNr(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "EpisodeNumber");
        Integer season = readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, "EpisodeNumber");
        return season;
    }

    private Integer readSeason(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "SeasonNumber");
        Integer season = readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, "SeasonNumber");
        return season;
    }

    private Integer readInt(XmlPullParser parser) throws IOException, XmlPullParserException {
        Integer result = null;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Integer.parseInt(parser.getText());
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
