package com.kaglainc.showtime.test;

import android.graphics.Rect;
import android.view.View;
import android.widget.Button;

import com.kaglainc.showtime.MainActivity;
import com.kaglainc.showtime.R;

/**
 * Created by Aleksander, Karl Erik, Glen on 09.12.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
public class MainActivityUnitTest extends
        android.test.ActivityInstrumentationTestCase2<MainActivity> {

    private Button friends;
    private MainActivity activity;
    private View mainLayout;

    public MainActivityUnitTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        activity = getActivity();
        mainLayout = activity.findViewById(R.id.mainLayout);
        friends = (Button) activity.findViewById(R.id.Friends);
    }

    public void testLayout() throws Exception {
        int buttonId1 = R.id.Friends;
        assertNotNull(activity.findViewById(buttonId1));
        Button testFriends = (Button) activity.findViewById(buttonId1);
        assertEquals("Incorrect label of the button", "Friends", testFriends.getText());

        int buttonId2 = R.id.Calendar;
        assertNotNull(activity.findViewById(buttonId2));
        Button calender = (Button) activity.findViewById(buttonId2);
        assertEquals("Incorrect label of the button", "Calendar", calender.getText());

        int logoId = R.id.thelogo;
        assertNotNull(activity.findViewById(logoId));
    }

    public void testFriendsButton() {
        int fullWidth = mainLayout.getWidth();
        int fullHeight = mainLayout.getHeight();
        int[] mainLayoutLocation = new int[2];
        mainLayout.getLocationOnScreen(mainLayoutLocation);


        int[] viewLocation = new int[2];
        friends.getLocationOnScreen(viewLocation);

        Rect outRect = new Rect();
        friends.getDrawingRect(outRect);

        assertTrue("Add button off the right of the screen", fullWidth
                + mainLayoutLocation[0] > outRect.width() + viewLocation[0]);

        assertTrue("Add button off the bottom of the screen", fullHeight
                + mainLayoutLocation[1] > outRect.height() + viewLocation[1]);
    }
}