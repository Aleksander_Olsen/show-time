package com.kaglainc.showtime.test;

import android.content.Intent;
import android.widget.TextView;

import com.kaglainc.showtime.R;
import com.kaglainc.showtime.Search;

/**
 * Created by Aleksander, Karl Erik, Glen on 09.12.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
public class SearchActivityUnitTest extends
        android.test.ActivityUnitTestCase<Search> {

    private Search activity;

    public SearchActivityUnitTest() {
        super(Search.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(),
                Search.class);
        startActivity(intent, null, null);
        activity = getActivity();
    }

    public void testSearchLayout() throws Exception {
        int textViewId;
        textViewId = R.id.error;
        assertNotNull(activity.findViewById(textViewId));
        TextView view = (TextView) activity.findViewById(textViewId);
        assertEquals("Incorrect label of the button", "No network connection", view.getText());

        int listId;
        listId = R.id.list;
        assertNotNull(activity.findViewById(listId));
    }
}
