package com.kaglainc.showtime;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link PersonalListFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class PersonalListFragment extends Fragment {

    private ArrayList<Episode> episodeList = new ArrayList<Episode>();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment PersonalListFragment.
     */
    public static PersonalListFragment newInstance() {
        PersonalListFragment fragment = new PersonalListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    public PersonalListFragment() {
        // Required empty public constructor
    }

    public void setEpisodeList(ArrayList<Episode> episodes){

        if(episodes != null) episodeList = episodes;
        else episodeList = new ArrayList<Episode>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this fragment across configuration changes.
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal_list, container, false);

        ListView listview = (ListView) view.findViewById(R.id.episodelist);
        listview.setAdapter(new CustomAdapterEpisode(getActivity(), episodeList, getResources()));

        return view;
    }

}
