package com.kaglainc.showtime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Aleksander, Karl Erik, Glen on 07.11.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
class CustomAdapterShow extends BaseAdapter implements View.OnClickListener{
    private final Activity activity;
    private final ArrayList data;
    private static LayoutInflater inflater = null;
    private final Resources res;

    public CustomAdapterShow(Activity activity, ArrayList data, Resources res) {
        this.activity = activity;
        this.data = data;
        this.res = res;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(data.size()<=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView showName;
        public TextView showOverview;
        public TextView showAired;
        public ImageView showImage;

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        ViewHolder holder;

        if(view==null) {

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.list_layout, viewGroup, false);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.showName = (TextView) vi.findViewById(R.id.showName);
            holder.showOverview = (TextView)vi.findViewById(R.id.showOverview);
            holder.showAired = (TextView)vi.findViewById(R.id.showAired);
            holder.showImage = (ImageView)vi.findViewById(R.id.showBanner);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        if(data.size()<=0) {
            holder.showName.setText(res.getText(R.string.noData));

        } else {
            /***** Get each Model object from Arraylist ********/
            Show tempValues;
            tempValues = ( Show ) data.get( i );

            /************  Set Model values in Holder elements ***********/

            holder.showName.setText( tempValues.getName() );
            if (tempValues.getOverview() != null) {
                holder.showOverview.setText(tempValues.getOverview());
            } else {
                holder.showOverview.setText(res.getText(R.string.noDescription));
            }
            if (tempValues.getFirstAired() != null) {
                holder.showAired.setText(res.getText(R.string.aired) + " "
                                     + tempValues.getFirstAired());
            } else {
                holder.showAired.setText(res.getText(R.string.aired) + " "
                                    + res.getString(R.string.unknown));
            }
            if(tempValues.getBanner() != null) {
                holder.showImage.setVisibility(View.VISIBLE);
                holder.showImage.setImageBitmap(tempValues.getBanner());
            } else {
                holder.showImage.setVisibility(View.GONE);
            }

            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new OnItemClickListener( i ));
        }
        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("CustomAdapterEpisode", "=====Row button clicked=====");
    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener {
        private final int mPosition;
        Show clicked = null;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {
            clicked = ( Show ) data.get( mPosition );
            Intent intent = new Intent(activity, DisplayShowActivity.class);
            intent.putExtra("id", clicked.getId().toString());
            activity.startActivity(intent);
        }
    }
}
