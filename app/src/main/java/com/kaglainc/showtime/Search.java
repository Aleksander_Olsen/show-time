package com.kaglainc.showtime;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * This is the search activity. This is called when the search widget is activated.
 * It takes the query from the user and adds it to the url.
 * It the executes an asynctask and parse the xml data.
 */
@SuppressWarnings("ConstantConditions")
public class Search extends Activity {
    private String URL = "";
    private final Search search = this;
    private TextView error;

    // The BroadcastReceiver that tracks network connectivity changes.
    private final NetworkReceiver receiver = new NetworkReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        error = (TextView) findViewById(R.id.error);
        error.setVisibility(View.GONE);

        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            setTitle(query);
            query = query.replace(" ", "+");
            URL = getResources().getText(R.string.search) + query;

            // Register BroadcastReceiver to track connection changes.
            IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            this.registerReceiver(receiver, filter);
        }
    }

    /**
     * OnStart it does a check to see if the app has access to internett.
     */
    @Override
    protected void onStart() {
        super.onStart();

        receiver.updateConnectedFlags(this);

        if (receiver.isConnected(this)) {
            // AsyncTask subclass
            new InitiateSearch().execute(URL);
        } else {
            error.setVisibility(View.VISIBLE);
            Toast.makeText(this, getResources().getText(R.string.connectionError),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            this.unregisterReceiver(receiver);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.display, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(Search.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_calendar) {
            startActivity(new Intent(getApplicationContext(), PersonalListActivity.class));
        } else if (id == R.id.action_friends) {
            startActivity(new Intent(getApplicationContext(), Friends.class));
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This is the AsyncTask that gets the xml data and runs it through the show parser.
     * It receives an arraylist with shows,
     * which it gives to a custom adapter to put in a listview
     */
    private class InitiateSearch extends AsyncTask<String, Void, ArrayList<Show>> {

        @Override
        protected ArrayList<Show> doInBackground(String... urls) {
            try {
                return loadXml(urls[0]);
            } catch (IOException e) {
                Log.e((String) getResources().getText(R.string.ioException), String.valueOf(e));
                return null;
            } catch (XmlPullParserException e) {
                Log.e((String) getResources().getText(R.string.xmlParseError), String.valueOf(e));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Show> s) {
            setContentView(R.layout.activity_search);

            ListView listView = (ListView) findViewById(R.id.list);
            Resources res = getResources();
            CustomAdapterShow adapter = new CustomAdapterShow(search, s, res);
            listView.setAdapter(adapter);
        }
    }

    private ArrayList<Show> loadXml(String urlString) throws XmlPullParserException, IOException {
        InputStream stream = null;
        ShowXmlParser showXmlParser = new ShowXmlParser();
        ArrayList<Show> shows = null;

        try {
            stream = downloadUrl(urlString);
            shows = showXmlParser.parse(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }

        return shows;
    }

    // Given a string representation of a URL, sets up a connection and gets
    // an input stream.
    private InputStream downloadUrl(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }
}