package com.kaglainc.showtime;

import android.graphics.Bitmap;

/**
 * Created by Aleksander, Karl Erik, Glen on 22.11.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
public class Episode {
    private final int id;
    private final int season;
    private final int episode;
    private final double rating;
    private final String name;
    private final String firstAired;
    private final String overview;
    private Bitmap poster;
    private int mSeries;
    private String seriesName;

    Episode(int _id, int _season, int _episode, double _rating,
            String _name, String _firstAired, String _overview, Bitmap _poster) {
        this.id          = _id;
        this.season      = _season;
        this.episode     = _episode;
        this.rating      = _rating;
        this.name        = _name;
        this.firstAired  = _firstAired;
        this.overview    = _overview;
        this.poster      = _poster;
    }

    public Episode(int id, int series, int season, int episode, String date, String name, String overview, double rating){
        this.id = id;
        this.mSeries = series;
        this.season = season;
        this.episode = episode;
        this.firstAired = date;
        this.name = name;
        this.overview = overview;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public int getSeason() {
        return season;
    }

    public int getEpisode() {
        return episode;
    }

    public double getRating() {
        return rating;
    }

    public String getName() {
        return name;
    }

    public String getFirstAired() {
        return firstAired;
    }

    public String getOverview() {
        return overview;
    }

    public Bitmap getPoster() {
        return poster;
    }

    public int getSeries() { return mSeries; }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }
}
