package com.kaglainc.showtime;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by Aleksander, Karl Erik, Glen on 11.11.2014.
 */
@SuppressWarnings({"ConstantConditions", "DefaultFileTemplate", "unchecked"})
class Data{
    private String[] url;
    private DataDownload dataDownload = null;
    private DownloadFinishedListener dlFinishedListener;
    private ArrayList<ArrayList> dataset = null;

    // The BroadcastReceiver that tracks network connectivity changes.
    private final NetworkReceiver receiver = new NetworkReceiver();

    public interface DownloadFinishedListener
    {
        void downloadFinished(Data data);
    }

    void downloadFinished(){
        if(dlFinishedListener != null) dlFinishedListener.downloadFinished(this);
    }

    public Data(){
        dataDownload = new DataDownload();
        dataset = new ArrayList<ArrayList>();


    }

    public void setURL(String[] url){
        this.url = url;
    }

    public ArrayList<ArrayList> getDataset(){
        return this.dataset;
    }

    public void setOnDataDownloadListener (DownloadFinishedListener listener)
    {
        // Store the listener object
        dlFinishedListener = listener;
    }

    /**
     * Execute the async task if device has valid connection. Send downloadFinished when finished
     * failed.
     * @param con
     */
    public void execute(Context con){
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        con.registerReceiver(receiver, filter);
        receiver.updateConnectedFlags(con);
        if(receiver.isConnected(con)) dataDownload.execute(url);
        else downloadFinished();

        if (receiver != null) {
            con.unregisterReceiver(receiver);
        }

    }


    /**
     * Download from external db.
     */
    @SuppressWarnings("unchecked")
    private class DataDownload extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls){
                StringBuilder sb = new StringBuilder();
                for (String params : urls) {

                    // Create httpclient
                    DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
                    String URL = "http://www.stud.hig.no/~110259/?" + params.replace(" ", "%20");
                    HttpPost httpPost = new HttpPost(URL);
                    httpPost.setHeader("Content-type", "text/xml");

                    InputStream inputStream;

                    try {

                        // Create parseable xml file.
                        HttpResponse response = httpClient.execute(httpPost);

                        HttpEntity entity = response.getEntity();

                        inputStream = entity.getContent();

                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

                        String line;

                        //Build xml string
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                        inputStream.close();

                        // Parse xml file and build the dataset out of tag + values.
                        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                        factory.setNamespaceAware(true);
                        XmlPullParser xpp = factory.newPullParser();
                        xpp.setInput(new StringReader(sb.toString()));
                        int eventType = xpp.getEventType();
                        int i = 0;
                        while (eventType != XmlPullParser.END_DOCUMENT) { //If not end of xml.
                            if ((eventType == XmlPullParser.START_TAG) && ((xpp.getName().equals("data")))) { //If requested starttag
                                dataset.add(i++, new ArrayList<DataObject>());
                            } else if (eventType == XmlPullParser.START_TAG && i > 0) {
                                dataset.get(i - 1).add(new DataObject(xpp.getName(), xpp.nextText()));
                            }

                            eventType = xpp.next();
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            return null;
        }

        protected void onPostExecute(String result){

            downloadFinished();
        }

    }


    /**
     * Get value of a requested tag given the correct additional tags
     * @param request The field you want.
     * @param index The field to compare
     * @param value The value to compare with index
     * @return requested tag where index = value, else null
     */
    public String get(String request, String index, String value){
        for(ArrayList<DataObject> objectList : dataset){
            String s = null;
            Boolean found = false;
            for(DataObject object : objectList){
                if(object.getTag().equals(index) && object.getValue().equals(value)) found = true;
                if(object.getTag().equals(request)) s = object.getValue();
            }
            if(found){
                return s;
            }
        }
        return null;
    }

    /**
     * Gets first tag that contains @request in List found at @list
     * @param request The field you want
     * @param list The list nr you want to search through
     * @return requested value of tag, or null.
     */
    public String get(String request, int list){
        if(dataset.size() >= list){
            for (DataObject object : (ArrayList<DataObject>) dataset.get(list)) {
                if (object.getTag().equals(request)) return object.getValue();
            }
        }

        return null;
    }

    /**
     * Dataobject og a tag + value combo.
     */
    private static class DataObject{
        final String tag;
        final String value;

        public DataObject(String tag, String value){
            this.tag = tag;
            this.value = value;
        }

        String getTag() {
            return this.tag;
        }

        String getValue() {
            return this.value;
        }
    }
}
