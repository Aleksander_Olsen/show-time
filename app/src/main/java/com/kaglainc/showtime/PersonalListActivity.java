package com.kaglainc.showtime;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;


@SuppressWarnings({"NullableProblems", "SameParameterValue"})
public class PersonalListActivity extends Activity {

    private CalendarFragment mCalendarFragment;
    private PersonalListFragment mListFragment;
    private boolean calendarActive;

    private ArrayList<Episode> mCurMonth;
    private Button modeChangeButton;

    // The BroadcastReceiver that tracks network connectivity changes.
    private final NetworkReceiver receiver = new NetworkReceiver();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_list);
        modeChangeButton = (Button) findViewById(R.id.mode_change);
        if (savedInstanceState == null) setCalendarFragment(new GregorianCalendar(Locale.ENGLISH));
        else{
            calendarActive = savedInstanceState.getBoolean("calendarActive", true);
            if(calendarActive) modeChangeButton.setBackgroundResource(R.drawable.ic_action_view_as_list);
            else modeChangeButton.setBackgroundResource(R.drawable.ic_action_view_as_grid);
        }

        modeChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modeChange();
            }
        });

        // Register BroadcastReceiver to track connection changes.
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver, filter);
        receiver.updateConnectedFlags(this);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("calendarActive", calendarActive);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //noinspection ConstantConditions
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        menu.findItem(R.id.action_calendar).setVisible(false);

        return true;
    }

    /**
     * Set active monthlist
     * @param episodes usually null
     */
    public void setCurMonth(ArrayList<Episode> episodes){
        this.mCurMonth = episodes;
    }

    public NetworkReceiver getReceiver(){
        return this.receiver;
    }

    /**
     * Add to current monthepisode list
     * @param dayEpisodes episodes for a day
     */
    public void addToMonth(ArrayList<Episode> dayEpisodes){
        if(mCurMonth == null) mCurMonth = new ArrayList<Episode>();
        mCurMonth.addAll(dayEpisodes);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(PersonalListActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.action_calendar){
            startActivity(new Intent(getApplicationContext(), PersonalListActivity.class));
        }
        else if(id == R.id.action_friends){
            startActivity(new Intent(getApplicationContext(), Friends.class));
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Set container to contain possibly retained CalendarFragment
     * @param calendar
     */
    void setCalendarFragment(Calendar calendar){

        if(mCalendarFragment == null){ // If not retained
            mCalendarFragment = new CalendarFragment();
            mCalendarFragment.setMember(calendar);
        }

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, mCalendarFragment);
        //transaction.addToBackStack(null);
        transaction.commit();

        modeChangeButton.setBackgroundResource(R.drawable.ic_action_view_as_list);

        calendarActive = true;

    }

    /**
     * Set fragment to contain a possibly retained ListFragment
     * @param episodeList episodelist to fill fragment listview
     */
    public void setListFragment(ArrayList<Episode> episodeList){

        if(mListFragment == null){ // If not retained
            mListFragment = new PersonalListFragment();
        }
        mListFragment.setEpisodeList(episodeList);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, mListFragment);

        transaction.commit();

        modeChangeButton.setBackgroundResource(R.drawable.ic_action_view_as_grid);

        calendarActive = false;
    }

    /**
     * Modechange button click
     */
    void modeChange(){
        if(!calendarActive){
            setCalendarFragment(new GregorianCalendar());
        }
        else{
            setListFragment(mCurMonth);
        }
    }
}
