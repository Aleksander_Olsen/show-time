package com.kaglainc.showtime;

import android.graphics.Bitmap;

/**
 * Created by Aleksander, Karl Erik, Glen on 02.11.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
public class Show {
    private final Integer id;
    private Integer seasons;
    private double rating;
    private final String name;
    private final String weekday;
    private final String firstAired;
    private final String genre;
    private final String overview;
    private final String status;
    private Bitmap poster;
    private Bitmap banner;

    public Show(Integer id, double rating, String name,
                String weekday, String firstAired, String genre,
                String overview, String status,
                Bitmap poster, Bitmap banner) {
        this.id = id;
        this.rating = rating;
        this.name = name;
        this.weekday = weekday;
        this.firstAired = firstAired;
        this.genre = genre;
        this.overview = overview;
        this.status = status;
        this.poster = poster;
        this.banner = banner;
    }

    public Show(Integer id, String firstAired, String weekday, String genre, String name,  String status, String overview){
        this.id = id;
        this.firstAired = firstAired;
        this.weekday = weekday;
        this.genre = genre;
        this.name = name;
        this.status = status;
        this.overview = overview;
    }

    public Integer getId() {
        return id;
    }

    public double getRating() {
        return rating;
    }

    public String getName() {
        return name;
    }

    public String getWeekday() {
        return weekday;
    }

    public String getFirstAired() {
        return firstAired;
    }

    public String getGenre() {
        return genre;
    }

    public String getOverview() {
        return overview;
    }

    public String getStatus() {
        return status;
    }

    public Bitmap getPoster() {
        return poster;
    }

    public Bitmap getBanner() {
        return banner;
    }

    public Integer getSeasons() { return seasons; }

    public void setSeasons(Integer seasons) { this.seasons = seasons; }
}
