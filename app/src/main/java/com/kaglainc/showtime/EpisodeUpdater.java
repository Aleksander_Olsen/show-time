package com.kaglainc.showtime;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Aleksander, Karl Erik, Glen on 09.12.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
class EpisodeUpdater extends AsyncTask<Show, Show, Void>{

    private final Calendar mCalendar;
    private final ArrayList<Show> shows;
    private final Context context;

    /**
     *
     * @param calendar containing month and year for updating
     * @param c context used for database.
     * @param shows tracked shows for user.
     */
    public EpisodeUpdater(Calendar calendar, Context c, ArrayList<Show> shows){
        if(calendar != null) this.mCalendar = (Calendar) calendar.clone();
        else this.mCalendar = new GregorianCalendar();
        this.context = c;
        this.shows = shows;
    }

    /**
     * Searches for all episodes this month and adds them to the database.
     * @param params the shows to search through
     * @return
     */
    @Override
    protected Void doInBackground(Show... params) {
        EpisodeXmlParser episodeParser = new EpisodeXmlParser();
        for(Show show : shows) {
                DataSource datasource = new DataSource(context);
                try {
                    datasource.open(); // Open database connection
                    datasource.deleteOldUpdates(); // Delete updates older than one week.
                    // Check if show is unupdated now.
                    if (!(datasource.getShowEpisodes(String.valueOf(show.getId()), String.valueOf(mCalendar.get(Calendar.MONTH) + 1), String.valueOf(mCalendar.get(Calendar.YEAR))))) {
                        // Update the calendar view with the new entries.
                        datasource.showEpisodesUpdated(show.getId(), mCalendar.get(Calendar.MONTH) + 1, mCalendar.get(Calendar.YEAR));
                        for (int i = 0; i < mCalendar.getMaximum(Calendar.DAY_OF_MONTH); i++) {
                            //Create the string used for date comparisons
                            String date = String.valueOf(mCalendar.get(Calendar.YEAR)) + "-" + String.valueOf(mCalendar.get(Calendar.MONTH) + 1) + "-" + (i + 1 < 10 ? "0" + String.valueOf(i + 1) : String.valueOf(i + 1));

                            InputStream stream = createStream(String.valueOf(show.getId()), date);

                            Episode episode;
                            if ((episode = episodeParser.parse(stream)) != null) {
                                datasource.saveEpisode(show.getId(), episode); // Saves episode to db
                            }

                        }
                    }

                    datasource.close(); // Closes databse connection.
                } catch (IOException e) {
                    Log.e("IO", "");
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    Log.e("XML", "");
                    e.printStackTrace();
                }

        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    /**
     * Create stream for episode on airdate search.
     * @param id of show
     * @param date to search for
     * @return inputStream
     * @throws IOException
     */
    private InputStream createStream(String id, String date) throws IOException {
        String urlString = "http://thetvdb.com/api/GetEpisodeByAirDate.php?apikey=E194B0AFF307E219&seriesid="  + id + "&airdate=" + date;
        java.net.URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }


}
