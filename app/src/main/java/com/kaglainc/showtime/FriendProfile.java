package com.kaglainc.showtime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;


public class FriendProfile extends Activity {

    private ArrayList<showAndRating> friendShow;


    /**
     * On creation of activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);
        Intent intent = getIntent();
        String userName = intent.getExtras().getString("name");
        TextView textView = (TextView) findViewById(R.id.nameProfile);
        textView.setText(userName + "'S SHOWS:");
        setupFriendShows(userName);
        friendShow = new ArrayList<showAndRating>();




    }

    /**
     * Find id of friend
     * @param na Nickname of friend
     */
    private void setupFriendShows(String na) {
        final String name = na;

        //Find id from username input in textfield
        final Data data = new Data();
        String params[] = {"operation=select&table=Users&nickname=" + name};
        data.setURL(params);
        data.setOnDataDownloadListener(new Data.DownloadFinishedListener() {
            @Override
            public void downloadFinished(Data finishedData) {

                String hey = finishedData.get("id", "nickname", name);
                findFriendShows(hey);

            }
        });
        data.execute(this);

    }

    /**
     * Find all shows and ratings from friend
     * @param hey Friend id
     */
    private void findFriendShows(String hey) {
        final Data data = new Data();
        String params[] = {"operation=select&table=UserSeries&user=" + hey + "&watched=1"};
        data.setURL(params);
        data.setOnDataDownloadListener(new Data.DownloadFinishedListener() {
            @Override
            public void downloadFinished(Data finishedData) {
                for (int i = 0; i < finishedData.getDataset().size(); i++){
                    String showw = finishedData.get("name", i);
                    String ratingg = finishedData.get("rating", i);
                    Float thisRating;

                    if (!ratingg.equals("")){
                        thisRating  = Float.parseFloat(ratingg);
                    } else{
                        thisRating = 0.0f;
                    }


                    friendShow.add(new showAndRating(showw, thisRating));



                }
                displayFriendShow();



            }
        });
        data.execute(this);

    }


    /**
     * Display all of your friend's shows
     */
    private void displayFriendShow() {
        CustomAdapterFriend adapter = new CustomAdapterFriend(this, friendShow);
        ListView list = (ListView) findViewById(R.id.friendshow);
        list.setAdapter(adapter);
    }

    /**
     * Custom adapter to show both name of show and ratingbar
     */
    public class CustomAdapterFriend extends ArrayAdapter<showAndRating>{

        public CustomAdapterFriend(Context context, ArrayList<showAndRating> shows) {
            super(context, 0, shows);

        }

        @Override
        public View getView(int position, View viewsomething, ViewGroup viewGroup){

            showAndRating shownrat = getItem(position);

            if (viewsomething == null) {
                viewsomething = LayoutInflater.from(getContext()).inflate(R.layout.friend_shows, viewGroup, false);
            }

            TextView textView = (TextView) viewsomething.findViewById(R.id.textView2);
            RatingBar ratingBar = (RatingBar) viewsomething.findViewById(R.id.indicator_ratingbar);

            textView.setText(shownrat.showwName);
            ratingBar.setRating(shownrat.rrating);

            return viewsomething;


        }
    }


    /**
     * Class that contains rating and showname
     */
    public class showAndRating {
        public final String showwName;
        public final Float rrating;

        public showAndRating(String showName, Float rating) {
            this.showwName = showName;
            this.rrating = rating;
        }
    }


    /**
     * On create options
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_friend_profile, menu);
        return true;
    }

    /**
     * On options selected
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
