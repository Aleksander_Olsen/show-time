package com.kaglainc.showtime;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Aleksander, Karl Erik, Glen on 02.11.2014.
 */
@SuppressWarnings({"ConstantConditions", "DefaultFileTemplate"})

/**
 * This is the xml parser for the shows.
 * This is used both in search function and the display function.
 */

class ShowXmlParser {
    private static final String ns = null;

    public ArrayList<Show> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    /**
     * This function starts to look for the "Data" tag in the xml document.
     * This indicates the start of the xml document.
     * Then it looks for the start tags "Series" and "Episode".
     * The xml document that is used to parse, contains all de information about the show.
     * That includes all the episodes in all the season that are available.
     * So, when it finds an episode tag, it looks for the season number.
     * It the gets the total number of seasons to be displayed in the spinnerlist of seasons.
     *
     * @param parser       The parser that contains the xml data.
     * @return             Returns a list of all the shows it found in the parser.
     * @throws XmlPullParserException
     * @throws IOException
     */
    private ArrayList<Show> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<Show> shows = new ArrayList<Show>();
        Show show = null;
        Integer season = null;

        parser.require(XmlPullParser.START_TAG, ns, "Data");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("Series")) {
                show = readEntry(parser);
            } else if(name.equals("Episode")) {
                season = readEpisode(parser);
            } else {
                skip(parser);
            }
            if (show != null) {
                show.setSeasons(season);
                shows.add(show);
            }
        }
        return shows;
    }

    /**
     * The read episode parser that looks for the number of season in a show.
     *
     * @param parser       The parser that contains the xml data.
     * @return             Returns the number of seasons.
     * @throws XmlPullParserException
     * @throws IOException
     */
    private Integer readEpisode(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "Episode");
        Integer number = null;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String parsed = parser.getName();

            if (parsed.equals("SeasonNumber")) {
                number = readSeason(parser);
            } else {
                skip(parser);
            }
        }

        return number;
    }

    /**
     * Reads through the xml data and retrieves the relevant data about the show.
     *
     * @param parser       The parser that contains the xml data.
     * @return             Returns a show object with all its data.
     * @throws XmlPullParserException
     * @throws IOException
     */
    private Show readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "Series");
        Integer id = null;
        double rating = 0;
        String name = null;
        String weekday = null;
        String firstAired = null;
        String genre = null;
        String overview = null;
        String status = null;
        String posterURL;
        String bannerURL;
        Bitmap poster = null;
        Bitmap banner = null;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String parsed = parser.getName();
            if (parsed.equals("id")) {
                id = readId(parser);
            } else if (parsed.equals("Rating")) {
                rating = readRating(parser);
            } else if (parsed.equals("SeriesName")) {
                name = readName(parser);
            } else if (parsed.equals("Airs_DayOfWeek")) {
                weekday = readWeekday(parser);
            } else if (parsed.equals("FirstAired")) {
                firstAired = readAired(parser);
            } else if (parsed.equals("Genre")) {
                genre = readGenre(parser);
            } else if (parsed.equals("Overview")) {
                overview = readOverview(parser);
            } else if (parsed.equals("Status")) {
                status = readStatus(parser);
            } else if (parsed.equals("poster")) {
                posterURL = readPoster(parser);
                String newURL = "http://thetvdb.com/banners/" + posterURL;
                try {
                    InputStream in = new java.net.URL(newURL).openStream();
                    poster = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    poster = null;
                }
            } else if (parsed.equals("banner")) {
                bannerURL = readBanner(parser);
                String newURL = "http://thetvdb.com/banners/" + bannerURL;
                try {
                    InputStream in = new java.net.URL(newURL).openStream();
                    banner = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    poster = null;
                }
            } else {
                skip(parser);
            }
        }

        return new Show(id, rating, name, weekday, firstAired, genre,
                            overview, status, poster, banner);
    }

    /**
     * From here there is individual readers for each of the relevant tags.
     *
     * @param parser       The parser that contains the xml data.
     * @return             Returns either a string, double or int for the current type.
     * @throws IOException
     * @throws XmlPullParserException
     */
    private Integer readId(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "id");
        Integer id = readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, "id");
        return id;
    }

    private Integer readSeason(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "SeasonNumber");
        Integer season = readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, "SeasonNumber");
        return season;
    }

    private double readRating(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Rating");
        double rating = readDouble(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Rating");
        return rating;
    }

    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "SeriesName");
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "SeriesName");
        return name;
    }

    private String readWeekday(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Airs_DayOfWeek");
        String weekday = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Airs_DayOfWeek");
        return weekday;
    }

    private String readAired(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "FirstAired");
        String aired = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "FirstAired");
        return aired;
    }

    private String readGenre(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Genre");
        String genre = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Genre");
        return genre;
    }

    private String readOverview(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Overview");
        String overview = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Overview");
        return overview;
    }

    private String readStatus(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Status");
        String status = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Status");
        return status;
    }

    private String readPoster(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "poster");
        String poster = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "poster");
        return poster;
    }

    private String readBanner(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "banner");
        String banner = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "banner");
        return banner;
    }

    /**
     *From here, individual parser for either a string, int or double.
     *
     * @param parser       The parser that contains the xml data.
     * @return             Returns either a string, double or int for the current type.
     * @throws IOException
     * @throws XmlPullParserException
     */
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = null;
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }

        return result;
    }

    private Integer readInt(XmlPullParser parser) throws IOException, XmlPullParserException {
        Integer result = null;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Integer.parseInt(parser.getText());
            parser.nextTag();
        }
        return result;
    }

    private double readDouble(XmlPullParser parser) throws IOException, XmlPullParserException {
        double result = 0.0;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Double.parseDouble(parser.getText());
            parser.nextTag();
        }
        return result;
    }

    /**
     * This is the skip for the tags we are not interested in.
     *
     * @param parser       The parser that contains the xml data.
     * @throws XmlPullParserException
     * @throws IOException
     */
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
