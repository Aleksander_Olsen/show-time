package com.kaglainc.showtime;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

/**
 * Created by Aleksander, Karl Erik, Glen on 04.12.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
public class PrefsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        if(!PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("currentUser", "none").equals("none")){
            addPreferencesFromResource(R.xml.userpreferences);
        }

        addPreferencesFromResource(R.xml.prefs);

        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        /*PreferenceScreen screen = getPreferenceScreen();
        screen.removePreference(findPreference("user"));*/
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /**
     * If shared preference has been changed
     * @param sharedPreferences
     * @param key To locate where change has been made
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("nickname")){
            Data data = new Data();
            String[] params = {"operation=update&table=Users&id=" + sharedPreferences.getString("currentUser", "none") + "&nickname=" + sharedPreferences.getString(key, null)};
            data.setURL(params);
            data.execute(getActivity());
        }


    }
}
