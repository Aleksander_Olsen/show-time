package com.kaglainc.showtime;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


@SuppressWarnings({"ConstantConditions", "UnusedParameters"})
public class Friends extends Activity {

    // getIntent() is a method from the started activity
    // Intent myIntent = getIntent(); // gets the previously created intent
    //String id = myIntent.getStringExtra("ID"); // will return "FirstKeyValue"
    private ArrayList<String> myFriend;
    private ArrayList<String> myPend;
    private String myID;

    // The BroadcastReceiver that tracks network connectivity changes.
    private final NetworkReceiver receiver = new NetworkReceiver();


    /**
     * When the activity is created
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        myID = prefs.getString("currentUser", "none");
        myFriend = new ArrayList<String>();
        myPend = new ArrayList<String>();

        // Register BroadcastReceiver to track connection changes.
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(receiver, filter);


        receiver.updateConnectedFlags(this);

        if (receiver.isConnected(this)) {
            fillListView(myID);
            fillPending(myID);
            registerClickPending();
            registerClickFriends();
        } else {
            Toast.makeText(this, getResources().getText(R.string.connectionError),
                    Toast.LENGTH_LONG).show();
        }


    }


    /**
     * When activity is closed
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            this.unregisterReceiver(receiver);
        }
    }


    /**
     * Get all friends from database
     * @param id id of friend
     */
    private void fillListView(final String id) {
        final Data data = new Data();
        String params[] = {"operation=select&table=Friends&user=" + id + "&status=1"};
        data.setURL(params);
        data.setOnDataDownloadListener(new Data.DownloadFinishedListener() {
            @Override
            public void downloadFinished(Data finishedData) {


                for (int i = 0; i < finishedData.getDataset().size(); i++){
                    if(finishedData.get("initiator", i).equals(id)){
                        if (finishedData.get("recNick", i)!= null){
                            setMyFriends(finishedData.get("recNick", i));
                        } else {
                            setMyFriends(finishedData.get("recName", i));
                        }

                    }
                    else if(finishedData.get("recipient", i).equals(id)){
                        if (finishedData.get("iniNick", i)!= null){
                            setMyFriends(finishedData.get("iniNick", i));
                        } else {
                            setMyFriends(finishedData.get("iniName", i));
                        }
                    } else{

                        setMyFriends("No friends");
                    }

                }

            }
        });
        data.execute(this);
    }


    /**
     * Get all pending friend requests from database
     * @param id Id of initiator
     */
    private void fillPending(final String id){

        final Data data = new Data();
        String params[] = {"operation=select&table=Friends&recipient=" + id +"&status=0"};
        data.setURL(params);
        data.setOnDataDownloadListener(new Data.DownloadFinishedListener() {
            @Override
            public void downloadFinished(Data finishedData) {

                for (int i = 0; i < finishedData.getDataset().size(); i++){
                    if (finishedData.get("iniNick", i)!= null) {
                        setPending(finishedData.get("iniNick", i));
                    } else{
                        setPending(finishedData.get("iniName", i));
                    }


                }

            }
        });
        data.execute(this);

    }

    /**
     * Button to send friend request
     * @param v View
     */
    public void buttonOnClick(View v){
        EditText sendName = (EditText) findViewById(R.id.editreq);
        String uname = sendName.getText().toString();


        //Find id from username input in textfield
        final Data data = new Data();
        String params[] = {"operation=select&table=Users&username=" + uname};
        data.setURL(params);
        final String uname2 = uname;
        data.setOnDataDownloadListener(new Data.DownloadFinishedListener() {
            @Override
            public void downloadFinished(Data finishedData) {

                String hey = finishedData.get("id", "username", uname2);

                sendRequest(hey);

            }
        });
        data.execute(this);
    }


    /**
     * Send request to user
     * @param idd Recipient id
     */
    void sendRequest(String idd){
        if (idd != null){
            final Data data = new Data();
            String params[] = {"operation=create&table=Friends&initiator=" + myID + "&recipient=" + idd};
            data.setURL(params);
            data.execute(this);
        } else {
            Log.e("ERROR", "Not a valid name!");
        }

    }

    /**
     * Add friends name to arraylist and listview
     * @param friendsHere name of friend
     */
    void setMyFriends(String friendsHere){
        myFriend.add(friendsHere);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, //Context
                R.layout.my_friends, //Layout
                myFriend); //Things to be displayed

        //Set the list view
        ListView list = (ListView) findViewById(R.id.friendlist);
        list.setAdapter(adapter);

    }

    /**
     * Add recipient name to arraylist and listview
     * @param pendingHere name of the recipient
     */
    void setPending(String pendingHere){
        myPend.add(pendingHere);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, //Context
                R.layout.my_pending, //Layout
                myPend); //Things to be displayed

        //Set the list view
        ListView list = (ListView) findViewById(R.id.pendinglist);
        list.setAdapter(adapter);

    }

    /**
     * Register if an element in the pendinglist has been clicked
     */
    private void registerClickPending(){
        ListView list = (ListView) findViewById(R.id.pendinglist);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                Log.e("HEYITWORKS", "HEYITWORKS!");
                almostApproved(textView.getText().toString());
            }
        });
    }

    /**
     * Register if an element in the friendslist has been clicked
     */
    private void registerClickFriends() {
        ListView list = (ListView) findViewById(R.id.friendlist);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                String sendAlong = textView.getText().toString();

                Intent i = new Intent(getApplicationContext(), FriendProfile.class);
                i.putExtra("name", sendAlong);
                startActivity(i);
            }
        });
    }

    /**
     * Get the id of the recipient
     * @param s Name of the recipient
     */
    private void almostApproved(String s) {
        //Find id from username input in textfield
        final Data data = new Data();
        String params[] = {"operation=select&table=Users&username=" + s};
        data.setURL(params);
        final String uname2 = s;

        data.setOnDataDownloadListener(new Data.DownloadFinishedListener() {
            @Override
            public void downloadFinished(Data finishedData) {

                String hey = finishedData.get("id", "username", uname2);
                approveRequest(hey);

            }
        });
        data.execute(this);

    }

    /**
     * Send approval of friend request
     * @param hey Id of the initiator of the friend request
     */
    private void approveRequest(String hey) {
        final Data data = new Data();
        String params[] = {"operation=update&table=Friends&initiator=" + hey + "&recipient=" + myID + "&status=1"};
        data.setURL(params);
        data.execute(this);
    }


    /**
     * Set up the options menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.friends, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        return true;
    }

    /**
     * Check what item on the actionbar is clicked
     * @param item The item that is clicked
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(Friends.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.action_calendar){
            startActivity(new Intent(getApplicationContext(), PersonalListActivity.class));
        }
        else if(id == R.id.action_friends){
            startActivity(new Intent(getApplicationContext(), Friends.class));
        }

        return super.onOptionsItemSelected(item);
    }
}
