package com.kaglainc.showtime;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Aleksander, Karl Erik, Glen on 23.11.2014.
 */
@SuppressWarnings({"ConstantConditions", "DefaultFileTemplate"})
/**
 * This is the xml parser for the episodes.
 * This parse all the relevant data about each individual episode.
 * This is used in the display seasons activity and the calender list.
 */
class EpisodeXmlParser {
    private static final String ns = null;

    public Episode parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private Episode readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        Episode episode = null;

        parser.require(XmlPullParser.START_TAG, ns, "Data");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if(name.equals("Episode")) {
                episode = readEntry(parser);
            }
            else if(name.equals(("Error"))){
                return null;
            }
            else {
                skip(parser);
            }
        }
        return episode;
    }

    private Episode readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "Episode");
        Integer id = null;
        Integer season = null;
        Integer episode = null;
        double rating = 0;
        String name = null;
        String firstAired = null;
        String overview = null;
        Bitmap poster = null;




        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String parsed = parser.getName();

            if (parsed.equals("id")) {
                id = readId(parser);
            }else if (parsed.equals("SeasonNumber")) {
                season = readSeason(parser);
            } else if (parsed.equals("EpisodeNumber")) {
                episode = readEpisode(parser);
            } else if (parsed.equals("Rating")) {
                rating = readRating(parser);
            } else if (parsed.equals("EpisodeName")) {
                name = readName(parser);
            } else if (parsed.equals("FirstAired")) {
                firstAired = readAired(parser);
            } else if (parsed.equals("Overview")) {
                overview = readOverview(parser);
            } else if (parsed.equals("filename")) {
                String posterURL = readPoster(parser);
                String newURL = "http://thetvdb.com/banners/" + posterURL;
                try {
                    InputStream in = new java.net.URL(newURL).openStream();
                    poster = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    poster = null;
                }
            } else {
                skip(parser);
            }
        }

        return new Episode(id, season, episode, rating, name, firstAired, overview, poster);
    }

    private Integer readId(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "id");
        Integer id = readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, "id");
        return id;
    }

    private Integer readSeason(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "SeasonNumber");
        Integer season = readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, "SeasonNumber");
        return season;
    }

    private Integer readEpisode(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "EpisodeNumber");
        Integer season = readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, "EpisodeNumber");
        return season;
    }

    private double readRating(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Rating");
        double rating = readDouble(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Rating");
        return rating;
    }

    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "EpisodeName");
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "EpisodeName");
        return name;
    }

    private String readAired(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "FirstAired");
        String aired = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "FirstAired");
        return aired;
    }

    private String readOverview(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "Overview");
        String overview = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Overview");
        return overview;
    }

    private String readPoster(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "filename");
        String poster = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "filename");
        return poster;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = null;
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }

        return result;
    }

    private Integer readInt(XmlPullParser parser) throws IOException, XmlPullParserException {
        Integer result = null;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Integer.parseInt(parser.getText());
            parser.nextTag();
        }
        return result;
    }

    private double readDouble(XmlPullParser parser) throws IOException, XmlPullParserException {
        double result = 0.0;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Double.parseDouble(parser.getText());
            parser.nextTag();
        }
        return result;
    }


    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
