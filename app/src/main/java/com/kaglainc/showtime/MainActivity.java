package com.kaglainc.showtime;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import java.util.ArrayList;
import java.util.Random;


@SuppressWarnings({"ConstantConditions", "NullableProblems", "UnusedParameters"})
public class MainActivity extends Activity implements
        ConnectionCallbacks, OnConnectionFailedListener, View.OnClickListener{

    //Default state of login system
    private static final int defaultLoginState = 0;
    //User has clicked the "sign in" button
    private static final int clickedSignIn = 1;
    //Started changing of state for resolving errors
    private static final int startedStateIntent = 2;
    private static final int signInUser = 0;
    private static final String savedState = "sign_in_progress";
    //Provide users access to google api
    private GoogleApiClient mGoogleApiClient;
    private int mSignInProgress;
    private PendingIntent mSignInIntent;


    private TextView mStatus;
    private TextView mNickname;

    private String ID;

    // The BroadcastReceiver that tracks network connectivity changes.
    private final NetworkReceiver receiver = new NetworkReceiver();


    /**
     * On creation of activity
     * @param savedInstanceState Remember last state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mStatus = (TextView) findViewById(R.id.sign_in_status);
        mNickname = (TextView) findViewById(R.id.nickname);
        ImageView mImageview = (ImageView) findViewById(R.id.thelogo);
        mImageview.setImageResource(R.drawable.ourlogo);


        // Register BroadcastReceiver to track connection changes.
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(receiver, filter);

        if (savedInstanceState != null) {
            mSignInProgress = savedInstanceState
                    .getInt(savedState, defaultLoginState);
        }

        receiver.updateConnectedFlags(this);

        if (receiver.isConnected(this)) {


            mGoogleApiClient = buildGoogleApiClient();
            mGoogleApiClient.connect();

        } else {
            mGoogleApiClient = null;
            Toast.makeText(this, getResources().getText(R.string.connectionError),
                    Toast.LENGTH_LONG).show();
        }

        final Button button = (Button) findViewById(R.id.btnSignIn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                signIn();
            }
        });

    }


    /**
     * Build the googleapiclient with the following permissions
     * @return
     */
    private GoogleApiClient buildGoogleApiClient() {
        // When we build the GoogleApiClient we specify where connected and
        // connection failed callbacks should be returned, which Google APIs our
        // app uses and which OAuth 2.0 scopes our app requests.
        return new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .build();
    }

    /**
     * Whenever activity starts up
     */
    @Override
    protected void onStart() {
        super.onStart();

        receiver.updateConnectedFlags(this);

        if (receiver.isConnected(this)) {
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient = null;
            Toast.makeText(this, getResources().getText(R.string.connectionError),
                    Toast.LENGTH_LONG).show();
        }

        createMyList();

    }

    /**
     * Whenever activity is shut down
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            this.unregisterReceiver(receiver);
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Create state
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(savedState, mSignInProgress);
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * When googleplus connection is established
     * @param connectionHint
     */
    @Override
    public void onConnected(Bundle connectionHint) {


        Person currentUser = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
        ID = currentUser.getId();

        findUsername();

        mSignInProgress = defaultLoginState;
    }


    /**
     * On googleplus connection failed
     * @param result
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {

        if (result.getErrorCode() == ConnectionResult.API_UNAVAILABLE) {
            Toast.makeText(getBaseContext(), getString(R.string.errorUnsupportedApi), Toast.LENGTH_SHORT).show();
            //Error message for not supported api should be placed here lol too tired though
        } else if (mSignInProgress != startedStateIntent) {

            mSignInIntent = result.getResolution();

            if (mSignInProgress == clickedSignIn) {
                resolveSignInError();
            }
        }

        mStatus.setText("User signed out");
    }

    /**
     * Establish connection to googleplus again
     */
    private void resolveSignInError() {
        if (mSignInIntent != null) {

            try {
                mSignInProgress = startedStateIntent;
                startIntentSenderForResult(mSignInIntent.getIntentSender(),
                        signInUser, null, 0, 0, 0);
            } catch (SendIntentException e) {
                mSignInProgress = clickedSignIn;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        switch (requestCode) {
            case signInUser:
                if (resultCode == RESULT_OK) {
                    mSignInProgress = clickedSignIn;
                } else {
                    mSignInProgress = defaultLoginState;
                }

                if (!mGoogleApiClient.isConnecting()) {
                    mGoogleApiClient.connect();
                }
                break;
        }
    }


    /**
     * When connection is suspended
     * @param cause Why
     */
    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }


    /**
     * Create options menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        return true;
    }

    //On options item selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this,
                    SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.action_calendar){
            startActivity(new Intent(getApplicationContext(), PersonalListActivity.class));
        }
        else if(id == R.id.action_friends){
            startActivity(new Intent(getApplicationContext(), Friends.class));
        }

        receiver.updateConnectedFlags(this);

        if (receiver.isConnected(this)) {

            switch (id) {

                case  R.id.action_sign_out:
                    if (!mGoogleApiClient.isConnecting() && mGoogleApiClient.isConnected()) {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                        if(prefs.getBoolean("revoke", false)){
                            revokeAccess();
                            prefs.edit().putBoolean("revoke", false).apply();
                        } else {
                            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                            mGoogleApiClient.disconnect();
                            mGoogleApiClient.connect();
                            prefs.edit().putString("currentUser", "none").apply();
                            mNickname.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Start calender activity
     * @param v
     */
    public void btnStartCalender(View v){
        startActivity(new Intent(getApplicationContext(), PersonalListActivity.class));
    }

    /**
     * Start friend activity
     * @param v
     */
    public void btnStartFriends(View v){
        startActivity(new Intent(getApplicationContext(), Friends.class));
    }

    /**
     * Find and set your user- and nickname
     */
    void findUsername(){
        Data data = new Data();
        String params[] = {"operation=select&table=Users"};
        data.setURL(params);
        data.setOnDataDownloadListener(new Data.DownloadFinishedListener() {
            @Override
            public void downloadFinished(Data finishedData) {

                if (finishedData.getDataset().size() > 0) {

                    String name;
                    if (finishedData.get("id", "id", ID) == null) {
                        name = newUsername(finishedData);
                    } else {
                        name = finishedData.get("username", "id", ID);
                    }

                    mStatus.setText("Signed in as " + name);
                    String nick;
                    mNickname.setText("Set name seen by friends in settings");
                    if ((nick = finishedData.get("nickname", "id", ID)) != null) {
                        if (!nick.equals("")) mNickname.setText("Seen as " + nick + " by friends");
                    }
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                    mNickname.setVisibility(View.VISIBLE);
                    pref.edit().putString("nickname", nick).apply();
                    pref.edit().putString("currentUser", ID).apply();
                }

                createMyList();
            }
        });
        data.execute(this);
    }

    /**
     * Create new username in database
     * @param userData
     * @return
     */
    String newUsername(Data userData){
        String name;
        // Set name as a new unique randomly generated username
        //noinspection StatementWithEmptyBody
        while((userData.get("id", "username", (name = createUsername())) != null));
        Data data = new Data();
        String user = name;
        String params[] = {"operation=create&table=Users&id=" + ID + "&username=" + user};
        data.setURL(params);
        data.execute(this);
        return name;
    }

    /**
     * Actually create a random username
     * @return Return username that is created
     */
    String createUsername(){
        // Arrays containing alphabet in lower and uppercase letters
        String[] charsLow = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",};
        String[] charsUpp = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",};

        //Generate username: Will only be useful once
        Random r = new Random();
        int i1 = r.nextInt(25);
        int i2 = r.nextInt(25);
        int i3 = r.nextInt(25);
        int i4 = r.nextInt(25);
        int i5 = r.nextInt(800);

        return charsUpp[i1] + charsLow[i2] + charsLow[i3] + charsUpp[i4] + Integer.toString(i5);

    }

    /**
     * Create list of my shows
     */
    void createMyList(){
        Data data = new Data();
        String[] params = {"operation=select&table=UserSeries&user=" + ID + "&watched=1"};
        data.setURL(params);
        data.setOnDataDownloadListener(new Data.DownloadFinishedListener() {
            @Override
            public void downloadFinished(Data data) {
                fillSeriesList(data);
            }
        });
        data.execute(this);
    }

    /**
     * Fill list of my shows
     * @param data
     */
    void fillSeriesList(Data data){
        ArrayList<Show> shows;
        DataSource datasource = new DataSource(this);
        datasource.open();
        shows = datasource.getShows();
        datasource.close();

        if(data.getDataset().size() != shows.size() && shows.size() > 0){
            ArrayList<String> dbShows = new ArrayList<String>();
            ArrayList<String> unknownShows = new ArrayList<String>();
            for(int i = 0; i < data.getDataset().size(); i++){
                dbShows.add(data.get("seriesId", i));
            }

            for(Show show : shows){
                if(!dbShows.remove(String.valueOf(show.getId()))){
                    unknownShows.add(String.valueOf(show.getId()));
                }
            }

            if(unknownShows.size() > 0 && ID != null){
                String params[] = new String[unknownShows.size()];
                int i = 0;
                for(String show : unknownShows){
                    params[i++] = "operation=create&table=UserSeries&user=" + ID + "&series=" + show + "&watched=1";
                }
                Data newData = new Data();
                newData.setURL(params);
                newData.execute(this);
            }
        }

        ListView list = (ListView) findViewById(R.id.myShowList);
        CustomAdapterShow adapter = new CustomAdapterShow(this, shows, getResources());
        list.setAdapter(adapter);

    }


    /**
     * Revoke all access to googleplus
     */
    void revokeAccess(){

        Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
        Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient);
        mGoogleApiClient = buildGoogleApiClient();
        mGoogleApiClient.connect();
        mNickname.setVisibility(View.GONE);

    }

    /**
     * Sign in with googleplus
     */
    void signIn(){
        if (receiver.isConnected(this)) {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = buildGoogleApiClient();
                mGoogleApiClient.connect();
            }
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
                resolveSignInError();
            }
        }

    }





}
