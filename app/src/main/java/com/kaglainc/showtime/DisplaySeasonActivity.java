package com.kaglainc.showtime;

import android.app.Activity;
import android.app.SearchManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class DisplaySeasonActivity extends Activity {
    private String URL = "";
    private String id  = "";
    private String seasonNr = "";
    private final DisplaySeasonActivity display = this;
    private final ArrayList<Episode> episodes = new ArrayList<Episode>();
    private TextView error;

    // The BroadcastReceiver that tracks network connectivity changes.
    private final NetworkReceiver receiver = new NetworkReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_season);

        error = (TextView) findViewById(R.id.error);
        id = getIntent().getStringExtra("id");
        seasonNr = getIntent().getStringExtra("season");
        setTitle(getIntent().getStringExtra("name"));
        URL = getResources().getText(R.string.apiPath) + id + "/all";

        // Register BroadcastReceiver to track connection changes.
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(receiver, filter);

        receiver.updateConnectedFlags(this);

        if (receiver.isConnected(this)) {
            // AsyncTask subclass
            new GetTotalEpisodes().execute(URL);
        } else {
            error.setVisibility(View.VISIBLE);
            Toast.makeText(this, getResources().getText(R.string.connectionError),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(receiver);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.display, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                                    // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(DisplaySeasonActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_calendar:
                startActivity(new Intent(getApplicationContext(), PersonalListActivity.class));
                break;
            case R.id.action_friends:
                startActivity(new Intent(getApplicationContext(), Friends.class));
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This is the asyncTask for the season activity.
     * It first goes through the EpisodeNrParser to find the number of episodes
     * in the season in question.
     * It then runs a for loop through all these and gets an url to the xml
     * with just the info about the individual episode.
     * It will then parse each episode and add it to the list to be shown.
     */
    private class GetTotalEpisodes extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... urls) {
            try {
                return loadNumberOfEpisodeXml(urls[0]);
            } catch (IOException e) {
                Log.e((String) getResources().getText(R.string.ioException), String.valueOf(e));
                return null;
            } catch (XmlPullParserException e) {
                Log.e((String) getResources().getText(R.string.xmlParseError), String.valueOf(e));
                return null;
            }
        }

        @Override
        protected void onPostExecute(Integer episodeNr) {
            int totalEpisodes = episodeNr;
            for(int i = 0; i <= totalEpisodes -1; i++) {
                URL = getResources().getText(R.string.apiPath) + id
                        + "/default/" + seasonNr + "/" + String.valueOf(i+1);
                new GetEpisodes().execute(URL);
            }
            if(totalEpisodes <= 0) {
                error.setText(getResources().getText(R.string.noData));
                error.setVisibility(View.VISIBLE);
            }
        }
    }

    private Integer loadNumberOfEpisodeXml(String urlString) throws XmlPullParserException, IOException {
        InputStream stream = null;
        EpisodeNrParser episodeXmlParser = new EpisodeNrParser();
        Integer ep = null;

        try {
            stream = downloadUrl(urlString);
            ep = episodeXmlParser.parse(stream, seasonNr);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }

        return ep;
    }

    private class GetEpisodes extends AsyncTask<String, Void, Episode> {

        @Override
        protected Episode doInBackground(String... urls) {
            try {
                return loadEpisodeXml(urls[0]);
            } catch (IOException e) {
                Log.e("ERROR", String.valueOf(e));
                return null;
            } catch (XmlPullParserException e) {
                Log.e("ERROR", String.valueOf(e));
                return null;
            }
        }

        @Override
        protected void onPostExecute(Episode episode) {
            if(episode != null) {
                episodes.add(episode);
            }

            setContentView(R.layout.activity_display_season);

            ListView listView = (ListView) findViewById(R.id.episodeList);
            Resources res = getResources();
            CustomAdapterEpisode adapter = new CustomAdapterEpisode(display, episodes, res);
            listView.setAdapter(adapter);
        }
    }

    private Episode loadEpisodeXml(String urlString) throws XmlPullParserException, IOException {
        InputStream stream = null;
        EpisodeXmlParser episodeXmlParser = new EpisodeXmlParser();
        Episode episode = null;

        try {
            stream = downloadUrl(urlString);
            episode = episodeXmlParser.parse(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }

        return episode;
    }

    // Given a string representation of a URL, sets up a connection and gets
    // an input stream.
    private InputStream downloadUrl(String urlString) throws IOException {
        java.net.URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }
}
