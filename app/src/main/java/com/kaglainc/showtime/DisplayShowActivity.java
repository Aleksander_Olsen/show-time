package com.kaglainc.showtime;

import android.app.Activity;
import android.app.SearchManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


@SuppressWarnings("ConstantConditions")
public class DisplayShowActivity extends Activity {
    private String id;
    private String URL = "";
    private Show show = null;
    private final DisplayShowActivity display = this;
    private TextView error;



    // The BroadcastReceiver that tracks network connectivity changes.
    private final NetworkReceiver receiver = new NetworkReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_show);

        Intent intent = getIntent();
        id = intent.getExtras().getString("id");
        error = (TextView) findViewById(R.id.error);
        error.setVisibility(View.GONE);
        URL = getResources().getText(R.string.apiPath) + id + "/all";

        // Register BroadcastReceiver to track connection changes.
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(receiver, filter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        receiver.updateConnectedFlags(this);

        if (receiver.isConnected(this)) {
            // AsyncTask subclass
            new GetShow().execute(URL);
        } else {
            DataSource datasource = new DataSource(this);
            datasource.open();
            show = datasource.getShow(Integer.valueOf(id));
            datasource.close();
            if(show != null) setUpUI();
            else {
                error.setVisibility(View.VISIBLE);
                Toast.makeText(this, getResources().getText(R.string.connectionError),
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
     protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            this.unregisterReceiver(receiver);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.display, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                                    // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(DisplayShowActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_calendar:
                startActivity(new Intent(getApplicationContext(), PersonalListActivity.class));
                break;
            case R.id.action_friends:
                startActivity(new Intent(getApplicationContext(), Friends.class));
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private class GetShow extends AsyncTask<String, Void, ArrayList<Show>> {

        @Override
        protected ArrayList<Show> doInBackground(String... urls) {
            try {
                return loadXml(urls[0]);
            } catch (IOException e) {
                Log.e((String) getResources().getText(R.string.ioException), String.valueOf(e));
                return null;
            } catch (XmlPullParserException e) {
                Log.e((String) getResources().getText(R.string.xmlParseError), String.valueOf(e));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Show> s) {
            setContentView(R.layout.activity_display_show);
            show = s.get(0);

            setTitle(show.getName());
            setUpUI();
        }
    }

    private ArrayList<Show> loadXml(String urlString) throws XmlPullParserException, IOException {
        InputStream stream = null;
        ShowXmlParser showXmlParser = new ShowXmlParser();
        ArrayList<Show> shows = null;

        try {
            stream = downloadUrl(urlString);
            shows = showXmlParser.parse(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }

        return shows;
    }

    // Given a string representation of a URL, sets up a connection and gets
    // an input stream.
    private InputStream downloadUrl(String urlString) throws IOException {
        java.net.URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();

        return conn.getInputStream();
    }

    void setTrackStatus(){
        findViewById(R.id.trackInfo).setVisibility(View.VISIBLE);
        CheckBox trackCheckBox = (CheckBox) findViewById(R.id.trackingCheck);

        //Check if user's tracking the show
        DataSource dataSource = new DataSource(DisplayShowActivity.this);
        dataSource.open();
        if (dataSource.getShow(show.getId()) != null) //Search local db and set button visibility
            trackCheckBox.setChecked(true);
        dataSource.close();

        trackCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) { //If supposed to be untracked
                    DataSource dataSource = new DataSource(DisplayShowActivity.this);
                    dataSource.open();
                    dataSource.deleteShow(show.getId());
                    dataSource.close();

                    saveToExDB("&watched=0");

                    Toast.makeText(DisplayShowActivity.this, "Removed" + " " + show.getName(), Toast.LENGTH_SHORT).show();


                } else { //If supposed to be tracked
                    DataSource dataSource = new DataSource(DisplayShowActivity.this);
                    dataSource.open();
                    Show newShow;
                    if ((newShow = dataSource.saveShow(show)) == null) {
                        Toast.makeText(DisplayShowActivity.this, getString(R.string.addError)
                                + " " + show.getName(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DisplayShowActivity.this, getString(R.string.added)
                                + " " + show.getName(), Toast.LENGTH_SHORT).show();
                    }
                    dataSource.close();

                    ArrayList<Show> shows = new ArrayList<Show>();

                    if (newShow != null) {
                        shows.add(newShow);
                    }

                    saveToExDB("&watched=1");

                    updateDB(shows);

                }
            }
        });
    }

    void setRating(){
        String curUser = PreferenceManager.getDefaultSharedPreferences(this).getString("currentUser", "none");
        if(!curUser.equals("none") && receiver.isConnected(this)){
            RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
            ratingBar.setVisibility(View.VISIBLE);
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    saveToExDB("&rating" + String.valueOf(rating));
                }
            });
        }
    }

    void updateDB(ArrayList<Show> shows) {

        EpisodeUpdater updater = new EpisodeUpdater(null, this, shows);
        updater.execute();

    }

    /**
     * Creates new entry if it doesn't exist, and updates it if it does.
     * @param change "&field=value"
     */
    void saveToExDB(String change){
        String curUser = PreferenceManager.getDefaultSharedPreferences(this).getString("currentUser", "none");
        if(!curUser.equals("none") && receiver.isConnected(this)) {
            String values = "&table=UserSeries&user=" + curUser + "&series=" + show.getId();
            String create = "operation=create" + values;
            String update = "operation=update" + values;
            String newShow = "operation=create&table=Series&series=" + String.valueOf(show.getId()) + "&name=" + show.getName();

            String params[] = {create, update + change, newShow};
            Data data = new Data();
            data.setURL(params);
            data.execute(this);
        }
    }

    void setUpUI() {
        ImageView poster = (ImageView) findViewById(R.id.displayPoster);
        TextView noImage = (TextView) findViewById(R.id.noImage);
        TextView title = (TextView) findViewById(R.id.displayTitle);
        TextView overview = (TextView) findViewById(R.id.displayOverview);
        TextView rating = (TextView) findViewById(R.id.displayRating);
        TextView status = (TextView) findViewById(R.id.displayStatus);
        TextView weekday = (TextView) findViewById(R.id.displayWeekday);
        TextView firstAired = (TextView) findViewById(R.id.displayFirstAired);
        TextView genre = (TextView) findViewById(R.id.displayGenre);
        if (show.getPoster() != null) {
            noImage.setVisibility(View.INVISIBLE);
            poster.setImageBitmap(show.getPoster());
        } else {
            noImage.setVisibility(View.VISIBLE);
        }
        title.setText(show.getName());
        if (show.getOverview() != null) {
            overview.setText(show.getOverview());
        } else {
            overview.setText(R.string.noDescription);
        }
        rating.setText(getText(R.string.rating) + String.valueOf(show.getRating()));
        if (show.getStatus() != null) {
            status.setText(getText(R.string.status) + show.getStatus());
        } else {
            status.setText(getText(R.string.status)
                    + String.valueOf(getText(R.string.unknown)));
        }
        if (show.getWeekday() != null && show.getStatus().equals("Continuing")) {
            weekday.setText(getText(R.string.weekday) + show.getWeekday());
            weekday.setVisibility(View.VISIBLE);
        }
        if (show.getFirstAired() != null) {
            firstAired.setText(getText(R.string.aired) + show.getFirstAired());
        } else {
            firstAired.setText(getText(R.string.aired)
                    + String.valueOf(getText(R.string.unknown)));
        }
        if (show.getGenre() != null) {
            String genres = show.getGenre().replace("|", ", ");
            genre.setText(genres.substring(2, genres.length() - 2));
        }
        if(show.getSeasons() != null) {
            ArrayList<String> seasons = new ArrayList<String>();
            for (int i = 0; i <= show.getSeasons()+1; i++) {
                if(i <= 0) {
                    seasons.add("SEASONS");
                } else if(i == 1) {
                    seasons.add("Specials");
                } else {
                    String line = "Season " + String.valueOf(i-1);
                    seasons.add(line);
                }
            }
            Spinner spinner = (Spinner) findViewById(R.id.spinner);
            spinner.setVisibility(View.VISIBLE);
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(display,
                    android.R.layout.simple_list_item_1, seasons);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView,
                                           int position, long id) {
                    if(position != 0) {
                        Intent intent = new Intent(display, DisplaySeasonActivity.class);
                        intent.putExtra("name", show.getName());
                        intent.putExtra("season", String.valueOf(position-1));
                        intent.putExtra("id", String.valueOf(show.getId()));
                        display.startActivity(intent);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    //DO NOTHING
                }
            });
        }

        setTrackStatus();
        setRating();
    }
}
