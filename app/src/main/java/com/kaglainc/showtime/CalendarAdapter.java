package com.kaglainc.showtime;

/**
 * Created by Aleksander, Karl Erik, Glen on 18.11.2014.
 */
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;

class CalendarAdapter extends BaseAdapter{
    private final String[] dayNames;

    private final Context mContext;
    private final Calendar mCalendar;
    private final Calendar mDate;
    private final ArrayList<ArrayAdapter> mItems;
    private final ArrayList<String> days;

    public CalendarAdapter(Context c, Calendar calendar){
        this.mCalendar = calendar;
        this.mDate = (Calendar) calendar.clone();
        this.mContext = c;
        this.mCalendar.set(Calendar.DAY_OF_MONTH, 1);
        this.mItems = new ArrayList<ArrayAdapter>();
        days = new ArrayList<String>();
        dayNames = new String[]{c.getResources().getString(R.string.monday),
                c.getResources().getString(R.string.tuesday),
                c.getResources().getString(R.string.wednesday),
                c.getResources().getString(R.string.thursday),
                c.getResources().getString(R.string.friday),
                c.getResources().getString(R.string.saturday),
                c.getResources().getString(R.string.sunday)};
        refreshDays();

    }

    public void addItems(int index, ArrayList<Episode> episodes){ mItems.add(new ArrayAdapter(index, episodes)); }

    /**
     *  Creates an item in gridview
     * @param position position in gridview
     * @param convertView
     * @param parent of gridview
     * @return the new gridview item
     */
    @SuppressLint("InflateParams")
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;
        TextView dayView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.calendar_item, null);

        }
        dayView = (TextView)v.findViewById(R.id.date);

        int date = 0;
        // disable empty days from the beginning
        if(!days.get(position).equals("") && !(Arrays.asList(dayNames)).contains(days.get(position))){
            date = Integer.valueOf(days.get(position));
        }
        else{
            v.setClickable(false);
            v.setFocusable(false);
        }
        // mark current day as focused
        if(mCalendar.get(Calendar.YEAR)== mDate.get(Calendar.YEAR) && mCalendar.get(Calendar.MONTH)== mDate.get(Calendar.MONTH) && days.get(position).equals( "" + mDate.get(Calendar.DAY_OF_MONTH))) {
            v.setBackgroundResource(R.drawable.item_background_focused);
        }
        else if(!Arrays.asList(dayNames).contains(days.get(position))){
            v.setBackgroundResource(R.drawable.list_item_background);
        }
        dayView.setText(days.get(position));



        TextView occurrence = (TextView)v.findViewById(R.id.date_container);
        occurrence.setText("");

        for(ArrayAdapter item : mItems){
            if(item.getDayIndex()+1 == date) occurrence.setText(String.valueOf(item.getEpisodes().size()));
            else if(item.getDayIndex() > date) break;
        }

        return v;
    }

    /**
     * Checks if @position should be enabled.
     * @param position of gridview item
     * @return true if not unenabled item.
     */
    @Override
    public boolean isEnabled(int position){
        return (!days.get(position).equals("") && !(Arrays.asList(dayNames)).contains(days.get(position)));

    }


    public int getCount() {
        return days.size();
    }

    /**
     * Get gridview item at @position
     * @param position of item
     * @return ArrayList<Episode> containing episodes that day.
     */
    public ArrayList<Episode> getItem(int position) {
        for(ArrayAdapter item : mItems){
            if(item.getDayIndex() == position) return item.getEpisodes();
            if(item.getDayIndex() > position) break;
        }
        return new ArrayList<Episode>();
    }

    public long getItemId(int position) {
        return 0;
    }

    /**
     * Builds a new list containing day elements.
     * Sets the look of the calendar for that month.
     */
    public void refreshDays(){

        this.mItems.clear(); // Clear item list
        days.clear();
        Calendar month = (Calendar) mCalendar.clone();
        int lastDay = month.getActualMaximum(Calendar.DAY_OF_MONTH);
        int firstDay = month.get(Calendar.DAY_OF_WEEK);

        int length;
        month.set(Calendar.DAY_OF_MONTH, lastDay);
        int FIRST_DAY_OF_WEEK = 1; //Start on monday
        if(firstDay == 1){
            length = lastDay+(FIRST_DAY_OF_WEEK *6);
        }
        else{
            length = lastDay+firstDay-(FIRST_DAY_OF_WEEK +1);
        }
        // Set correct startday in month.
        if(month.get(Calendar.DAY_OF_WEEK) != 7/(FIRST_DAY_OF_WEEK *6+1)) length += 7 - (month.get(Calendar.DAY_OF_WEEK) - FIRST_DAY_OF_WEEK);

        int calStart = firstDay - FIRST_DAY_OF_WEEK > 0 ? firstDay - FIRST_DAY_OF_WEEK : 7;

        //Add the daynames on the top of the calendar
        Collections.addAll(days, dayNames);

        //Add blank days or numbers
        for(int i=1; i <= length; i++){
            if(i < calStart || i >= lastDay + calStart) days.add("");
            else{

                days.add(String.valueOf(i + 1 - calStart));

            }
        }
    }

    /**
     * Contains episodelist indexed on days.
     */
    class ArrayAdapter{
        final int mDayIndex;
        final ArrayList<Episode> mEpisodes;

        public ArrayAdapter(int index, ArrayList<Episode> episodes){
            mDayIndex = index;
            mEpisodes = episodes;
        }

        public int getDayIndex(){
            return this.mDayIndex;
        }

        public ArrayList<Episode> getEpisodes(){
            return this.mEpisodes;
        }
    }

}
