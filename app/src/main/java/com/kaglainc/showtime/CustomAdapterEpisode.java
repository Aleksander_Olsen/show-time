package com.kaglainc.showtime;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Aleksander, Karl Erik, Glen on 02.12.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
class CustomAdapterEpisode extends BaseAdapter implements View.OnClickListener{
    private final ArrayList data;
    private static LayoutInflater inflater = null;
    private final Resources res;

    public CustomAdapterEpisode(Activity activity, ArrayList data, Resources res) {
        this.data = data;
        this.res = res;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(data.size()<=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView epEpisode;
        public TextView epName;
        public TextView epRating;
        public TextView epFirstAired;
        public TextView epOverview;
        public ImageView epPoster;

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        ViewHolder holder;

        if(view==null) {

            vi = inflater.inflate(R.layout.list_episode_layout, viewGroup, false);

            holder = new ViewHolder();
            holder.epEpisode = (TextView) vi.findViewById(R.id.epEpisode);
            holder.epName = (TextView) vi.findViewById(R.id.epName);
            holder.epRating = (TextView) vi.findViewById(R.id.epRating);
            holder.epFirstAired = (TextView) vi.findViewById(R.id.epFirstAired);
            holder.epOverview = (TextView) vi.findViewById(R.id.epOverview);
            holder.epPoster = (ImageView) vi.findViewById(R.id.epPoster);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        if(data.size()<=0) {
            holder.epName.setText(res.getText(R.string.noData));

        } else {
            /***** Get each Model object from Arraylist ********/
            Episode tempValues;
            tempValues = ( Episode ) data.get( i );

            /************  Set Model values in Holder elements ***********/

            holder.epName.setText(tempValues.getName());
            String show = "";
            String tempS;
            String tempE;
            if(tempValues.getSeriesName() != null){
                show = tempValues.getSeriesName() + " - ";
            }
            if(tempValues.getSeason() < 10) {
                tempS = "0" + String.valueOf(tempValues.getSeason());
            } else {
                tempS = String.valueOf(tempValues.getSeason());
            }
            if(tempValues.getEpisode() < 10) {
                tempE = "0" + String.valueOf(tempValues.getEpisode());
            } else {
                tempE = String.valueOf(tempValues.getEpisode());
            }
            String se = show + "S" + tempS + "E" + tempE;
            holder.epEpisode.setText(se);
            if(tempValues.getPoster() != null) {
                holder.epPoster.setImageBitmap(tempValues.getPoster());
            } else {
                holder.epPoster.setVisibility(View.GONE);
            }
            if (tempValues.getOverview() != null) {
                holder.epOverview.setText(tempValues.getOverview());
            } else {
                holder.epOverview.setText(res.getText(R.string.noDescription));
            }
            holder.epRating.setText(res.getText(R.string.rating) + String.valueOf(tempValues.getRating()));
            if (tempValues.getFirstAired() != null) {
                holder.epFirstAired.setText(res.getText(R.string.aired) + tempValues.getFirstAired());
            } else {
                holder.epFirstAired.setText(res.getText(R.string.aired)
                        + String.valueOf(res.getText(R.string.unknown)));
            }

            /******** Set Item Click Listner for LayoutInflater for each row *******/
            vi.setOnClickListener(new OnItemClickListener( i, holder ));
        }
        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("CustomAdapterEpisode", "=====Row button clicked=====");
    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener {
        private final int mPosition;
        private Episode clicked = null;
        private final ViewHolder nowHolder;

        OnItemClickListener(int position, ViewHolder _holder){
            nowHolder = _holder;
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {
            clicked = ( Episode ) data.get( mPosition );
            if(nowHolder.epOverview.getVisibility() == View.VISIBLE) {
                nowHolder.epRating.setVisibility(View.GONE);
                nowHolder.epFirstAired.setVisibility(View.GONE);
                nowHolder.epOverview.setVisibility(View.GONE);
                nowHolder.epPoster.setVisibility(View.GONE);
            } else {
                if(clicked.getPoster() != null) {
                    nowHolder.epPoster.setVisibility(View.VISIBLE);
                } else {
                    nowHolder.epPoster.setVisibility(View.GONE);
                }
                nowHolder.epRating.setVisibility(View.VISIBLE);
                nowHolder.epFirstAired.setVisibility(View.VISIBLE);
                nowHolder.epOverview.setVisibility(View.VISIBLE);
            }
        }
    }
}
