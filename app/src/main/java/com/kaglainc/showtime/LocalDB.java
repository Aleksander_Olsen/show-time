package com.kaglainc.showtime;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Aleksander, Karl Erik, Glen on 27.11.2014.
 */
@SuppressWarnings("DefaultFileTemplate")
class LocalDB extends SQLiteOpenHelper{

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_OVERVIEW = "overview";
    public static final String COLUMN_FIRSTAIRED = "firstAired";
    public static final String COLUMN_UPDATED = "updated";
    public static final String COLUMN_RATING = "rating";

    public static final String TABLE_SHOWS = "Shows";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_AIRING = "airing";
    public static final String COLUMN_GENRE = "genre";


    public static final String TABLE_EPISODES = "Episodes";
    public static final String COLUMN_SEASON = "season";
    public static final String COLUMN_EPISODE = "episode";
    public static final String COLUMN_SERIES = "series";

    public static final String TABLE_SHOWEPISODES = "ShowEpisodes";
    public static final String COLUMN_MONTH = "month";
    public static final String COLUMN_YEAR = "year";

    private static final String DATABASE_NAME = "showtime.db";
    private static final int DATABASE_VERSION = 32;

    // Database creation sql statement
    private static final String EPISODES_CREATE = "CREATE TABLE "
            + TABLE_EPISODES + "("
            + COLUMN_ID + " BIGINT PRIMARY KEY, "
            + COLUMN_SERIES + " BIGINT, "
            + COLUMN_SEASON + " INTEGER, "
            + COLUMN_EPISODE + " INTEGER, "
            + COLUMN_FIRSTAIRED + " TEXT, "
            + COLUMN_NAME + " TEXT, "
            + COLUMN_OVERVIEW + " NVARCHAR(4000), "
            + COLUMN_RATING + " FLOAT, "
            + COLUMN_UPDATED + " DATETIME DEFAULT(STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')))";

    private static final String SHOWS_CREATE = "CREATE TABLE "
            + TABLE_SHOWS + "("
            + COLUMN_ID + " bigint primary key, "
            + COLUMN_FIRSTAIRED + " TEXT, "
            + COLUMN_AIRING + " TEXT, "
            + COLUMN_GENRE + " TEXT, "
            + COLUMN_NAME + " TEXT, "
            + COLUMN_STATUS + " TEXT, "
            + COLUMN_OVERVIEW + " NVARCHAR(4000))";

    private static final String SHOWEPISODES_CREATE = "CREATE TABLE "
            + TABLE_SHOWEPISODES + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
            + COLUMN_SERIES + " bigint, "
            + COLUMN_MONTH + " int, "
            + COLUMN_YEAR + " int, "
            + COLUMN_UPDATED + " DATETIME DEFAULT(STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')))";

    public LocalDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e("Creating episodetable", EPISODES_CREATE);
        db.execSQL(EPISODES_CREATE);
        Log.e("Creating showtable", SHOWS_CREATE);
        db.execSQL(SHOWS_CREATE);
        Log.e("Creating showepisodes", SHOWEPISODES_CREATE);
        db.execSQL(SHOWEPISODES_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
        Log.w(LocalDB.class.getName(),
                "Upgrading database from version " + oldVer + " to "
                        + newVer + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EPISODES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOWS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOWEPISODES);
        onCreate(db);

    }
}

class DataSource{
    private SQLiteDatabase database;
    private final LocalDB dbHelper;

    private final String[] episodesColumns = { LocalDB.COLUMN_ID, LocalDB.COLUMN_SERIES, LocalDB.COLUMN_SEASON, LocalDB.COLUMN_EPISODE,
            LocalDB.COLUMN_FIRSTAIRED, LocalDB.COLUMN_NAME, LocalDB.COLUMN_OVERVIEW, LocalDB.COLUMN_RATING};
    private final String[] showColumns = {LocalDB.COLUMN_ID, LocalDB.COLUMN_FIRSTAIRED, LocalDB.COLUMN_AIRING, LocalDB.COLUMN_GENRE, LocalDB.COLUMN_NAME, LocalDB.COLUMN_STATUS, LocalDB.COLUMN_OVERVIEW};

    public DataSource(Context context) {
        dbHelper = new LocalDB(context);
    }

    public void open() { database = dbHelper.getWritableDatabase(); }

    public void close() {
        dbHelper.close();
    }

    /**
     * Save episode to db
     * @param show id for episode show.
     * @param episode to save.
     */
    public void saveEpisode(int show, Episode episode){
        try{
            //Create values to save.
            ContentValues values = new ContentValues();
            values.put(LocalDB.COLUMN_ID, episode.getId());
            values.put(LocalDB.COLUMN_SERIES, show);
            values.put(LocalDB.COLUMN_SEASON, episode.getSeason());
            values.put(LocalDB.COLUMN_EPISODE, episode.getEpisode());
            values.put(LocalDB.COLUMN_FIRSTAIRED, episode.getFirstAired());
            values.put(LocalDB.COLUMN_NAME, episode.getName());
            values.put(LocalDB.COLUMN_OVERVIEW, episode.getOverview());
            values.put(LocalDB.COLUMN_RATING, episode.getRating());
            database.insert(LocalDB.TABLE_EPISODES, null, values);

            //Get the newly added entry.
            Cursor cursor = database.query(LocalDB.TABLE_EPISODES,
                    episodesColumns, LocalDB.COLUMN_ID + " = '" + episode.getId() + "'", null,
                    null, null, null);
            cursor.moveToFirst();
            cursor.close();
        }catch(Exception e){
            Log.e("ERROR", String.valueOf(e));
        }
    }


    /**
     * Get all episodes based on date
     * @param date to search
     * @return arraylist of found episodes.
     */
    public ArrayList<Episode> getEpisodesByDate(String date) {
        ArrayList<Episode> episodes = new ArrayList<Episode>();

        String whereClause = LocalDB.COLUMN_FIRSTAIRED + "= ?";

        String[] whereArgs = new String[] { date };
        Cursor cursor;

        try {
            cursor = database.query(LocalDB.TABLE_EPISODES,
                    episodesColumns, whereClause, whereArgs, null, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Episode episode = cursorToEpisode(cursor);
                episodes.add(episode);
                cursor.moveToNext();
            }
            cursor.close();
            return episodes;
        }
        catch(Exception ex){
            Log.e("Error:", ex.getMessage());
        }

        return new ArrayList<Episode>();
    }

    private Episode cursorToEpisode(Cursor cursor){
        return new Episode(
                cursor.getInt(0), //id
                cursor.getInt(1), //series
                cursor.getInt(2), //seasonNr
                cursor.getInt(3), //episodeNr
                cursor.getString(4), //date
                cursor.getString(5), //name
                cursor.getString(6), //overview
                cursor.getFloat(7)); //rating
    }


    /**
     * Save a single show
     * @param show to be saved
     * @return
     */
    public Show saveShow(Show show){
        try {
            //Create values to save.
            ContentValues values = new ContentValues();
            values.put(LocalDB.COLUMN_ID, show.getId());
            values.put(LocalDB.COLUMN_FIRSTAIRED, show.getFirstAired());
            values.put(LocalDB.COLUMN_AIRING, show.getWeekday());
            values.put(LocalDB.COLUMN_GENRE, show.getGenre());
            values.put(LocalDB.COLUMN_NAME, show.getName());
            values.put(LocalDB.COLUMN_STATUS, show.getStatus());
            values.put(LocalDB.COLUMN_OVERVIEW, show.getOverview());

            database.insert(LocalDB.TABLE_SHOWS, null, values);

            return getShow(show.getId());
        } catch (Exception e){
            Log.e("ERROR", String.valueOf(e));
        }
        return null;
    }

    /**
     * Get a single show
     * @param id show to get
     * @return found show if any.
     */
    public Show getShow(int id){
        Cursor cursor = database.query(LocalDB.TABLE_SHOWS,
                showColumns, LocalDB.COLUMN_ID + " = '" + id + "'", null,
                null, null, null);
        cursor.moveToFirst();
        Show newShow = cursorToShow(cursor);
        cursor.close();
        return newShow;
    }

    /**
     * Delete show and all data connected to that show.
     * @param id show to be deleted.
     */
    public void deleteShow(int id){
        database.delete(LocalDB.TABLE_SHOWS, LocalDB.COLUMN_ID + " = " + id, null);
        database.delete(LocalDB.TABLE_EPISODES, LocalDB.COLUMN_SERIES + " = " + id, null);
        database.delete(LocalDB.TABLE_SHOWEPISODES, LocalDB.COLUMN_SERIES + " = " + id, null);
    }

    /**
     * Get all shows in db
     * @return all shows in db
     */
    public ArrayList<Show> getShows(){
        ArrayList<Show> shows = new ArrayList<Show>();

        Cursor cursor = null;

        try {
            cursor = database.query(LocalDB.TABLE_SHOWS,
                    showColumns, null, null, null, null, null, null);
        }
        catch(Exception ex){
            Log.e("Error:", "couldn't create cursor");
        }
        if(cursor != null){
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Show show = cursorToShow(cursor);
                shows.add(show);
                cursor.moveToNext();
            }
            cursor.close();
            return shows;
        }

        return new ArrayList<Show>();
    }

    /**
     * Change cursor to a show object.
     * @param cursor to be changed
     * @return new show object or null if empty cursor
     */
    private Show cursorToShow(Cursor cursor) {
        if(cursor.getCount() > 0) return new Show(
                cursor.getInt(0), //id
                cursor.getString(1), //firstaired
                cursor.getString(2), //weekday
                cursor.getString(3), //genre
                cursor.getString(4), //name
                cursor.getString(5), //status
                cursor.getString(6));//overview
        else return null;
    }

    /**
     * A show has gained new episodes
     * @param show that is updated
     * @param month that is updated
     * @param year that is updated
     */
    public void showEpisodesUpdated(int show, int month, int year){

        try {

            ContentValues values = new ContentValues();
            values.put(LocalDB.COLUMN_SERIES, show);
            values.put(LocalDB.COLUMN_MONTH, month);
            values.put(LocalDB.COLUMN_YEAR, year);

            database.insert(LocalDB.TABLE_SHOWEPISODES, null, values);

        } catch (Exception e){
            Log.e("ERROR", String.valueOf(e));
        }

    }

    /**
     * Delete all updates older than 7 days
     */
    public void deleteOldUpdates(){
        String where = LocalDB.COLUMN_UPDATED + " < ? ";
        String args[] = {"(STRFTIME('%Y-%m-%d %H:%M:%f', ('now','-7 day')))"};
        database.delete(LocalDB.TABLE_SHOWEPISODES, where, args);
        database.delete(LocalDB.TABLE_EPISODES, where, args);
    }

    /**
     * Check if show is updated
     * @param show to check
     * @param month to check
     * @param year to check
     * @return true if an update is found.
     */
    public boolean getShowEpisodes(String show, String month, String year){

        String[] columns = {LocalDB.COLUMN_UPDATED};
        String where = LocalDB.COLUMN_SERIES + " = ? AND "
                + LocalDB.COLUMN_MONTH + " = ? AND "
                + LocalDB.COLUMN_YEAR + " = ?";
        String[] whereArgs = new String[] { show, month, year };
        try {
            Cursor cursor = database.query(LocalDB.TABLE_SHOWEPISODES, columns, where, whereArgs, null, null, null, null);
            cursor.moveToFirst();
            if(!cursor.isAfterLast()) return true;
        } catch(Exception e){
            Log.e("ERROR", String.valueOf(e));
        }

        return false;
    }
}
