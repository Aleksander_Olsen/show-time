<?php
    $values = array();
    if(isset($_GET["operation"])) $op = $_GET["operation"];
    if(isset($_GET["table"])) $table = $_GET["table"];
    if(isset($_GET["user"])) $values[":user"] = $_GET["user"];
    if(isset($_GET["series"])) $values[":seriesId"] = $_GET["series"];
    if(isset($_GET["rating"])) $values[":rating"] = $_GET["rating"];
    if(isset($_GET["watched"])) $values[":watched"] = $_GET["watched"];
    if(isset($_GET["id"])) $values[":id"] = $_GET["id"];
    if(isset($_GET["body"])) $values[":body"] = str_replace("%20", " ", $_GET["body"]);
    if(isset($_GET["username"])) $values[":username"] = str_replace("%20", " ", $_GET["username"]);
    if(isset($_GET["initiator"])) $values[":initiator"] = $_GET["initiator"];
    if(isset($_GET["recipient"])) $values[":recipient"] = $_GET["recipient"];
    if(isset($_GET["status"])) $values[":status"] = $_GET["status"];
    if(isset($_GET["nickname"])) $values[":nickname"] = $_GET["nickname"];
    if(isset($_GET["name"])) $values[":name"] = $_GET["name"];


    $xml = "";
    $response = "";

	if(isset($op) && isset($table)) {
        if ($op == "create") { // Create insert statement.
            $query = "INSERT IGNORE INTO " . $table;

            $attrList = "";
            $valNames = array_keys($values);
            foreach($valNames as $valName){
                if($attrList !== '') $attrList .= ", ";
                $attrList .= $valName;
            }

            $query .= "(" . str_replace(":", "", $attrList) . ") VALUES(" . $attrList . ")";
			//echo $query;
        }
        else if ($op == "update" /*&& isset($user) && isset($seriesId)*/) { // Create update statement.
            $query = "UPDATE " . $table;
            $set = "";
            $where = "";
            $valNames = array_keys($values);
            foreach($valNames as $valName){
                if($valName == ":user" || $valName == ":seriesId" || $valName == ":id" || $valName == ":initiator" || $valName == ":recipient"){
                    if($where !== '') $where .= " AND ";
                    $where .= str_replace(":", "", $valName) . "= " . $valName;
                }
                else{
                    if($set !== '') $set .= ", ";
                    $set .= str_replace(":", "", $valName) . "= " . $valName;
                }
            }
            $set = " SET " . $set;
            $where = " WHERE " . $where;
            $query .= $set . $where;
            //echo "<test>" . $query . "</test>";

        }
		else if ($op == "delete"){
		    $query = "DELETE FROM " . $table;
			$where = "";
			
			$valNames = array_keys($values);
			foreach($valNames as $valName) {
                if ($where !== '') $where .= " AND ";
				$where .= str_replace(":", "", $valName) . "= " . $valName;
			}
			
			$where = " WHERE " . $where;
			$query .= $where;
			
			//echo "<test>" . $query . "</test>";
			
		}
        else if ($op == "select") { // Create select statement.
            $query = "SELECT * FROM " . $table;

            $where = "";

            $valNames = array_keys($values);

			if($where == ""){
                foreach($valNames as $valName) {
                    if ($where !== '') $where .= " AND ";
                    $splitArray = array_reverse(preg_split('#(?<=<|>)(?=\d)#', $values[$valName]));
                    $where .= str_replace(":", "", $valName) . (isset($splitArray[1]) ? $splitArray[1] : "") . "= " . $valName;
				    $values[$valName] = $splitArray[0];
                }
			}
			
			if($table == "Friends"){
                $query = "SELECT F.initiator, F.recipient, 
				          U1.username AS iniName, U1.nickname AS iniNick,
						  U2.username AS recName, U2.nickname AS recNick, 
						  F.status
                          FROM Friends AS F
                          INNER JOIN Users AS U1
                            ON F.initiator = U1.id
                          INNER JOIN Users AS U2
                            ON F.recipient = U2.id";
			    if(isset($values[":user"])) $where = "(F.initiator = :user OR F.recipient = :user) AND F.status = 1";
			}
            else if($table == "UserSeries"){
                $query = "SELECT US.user, US.seriesId, S.name, US.rating, US.watched
                          FROM UserSeries AS US
                          INNER JOIN Series AS S
                            ON S.id = US.seriesId AND S.locale = US.locale";
            }

            $query .= ($where !== '' ? " WHERE ".$where : "");
            //echo "<test>" . $query . "</test>";
        }

        try {
            $dbh = new PDO('mysql:host=localhost;dbname=s110259; charset=utf8', "s110259", "bTZ4GUWO");
            //echo $query;
            $stmt = $dbh->prepare($query);
            $stmt->execute($values);
            if($op == "select"){
                if($stmt->rowCount()) {
                    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $data) {
                        $attrNames = array_keys($data);
                        $xml .= "<data>";
                        foreach ($attrNames as $attrName) {
                            $xml .= "<" . $attrName . ">" . $data[$attrName] . "</" . $attrName . ">";
                        }
                        $xml .="</data>";
                    }
                } else $response = "<Error>3|No data found</Error>";
            }
        }
        catch(exception $e){
            $response = "<Error>2|Couldn't connect to database</Error>";
        }

    } else $response = "<Error>1|Couldn't do the operation. Incorrect command or missing values</Error>";

	$pre = "<xml>".$response;
	$xml = $pre . $xml;
	$xml .= "</xml>";
	header('Content-Type:text/xml; charset=UTF-8');

	echo $xml;

	